from tkinter import *
from random import randint

def magic(event):
    global rand_x, rand_y

    if str(event.type) == 'Enter':
        rand_x = randint(0 + 100, width - 100)
        rand_y = randint(0 + 100, height - 100)

        btn.place(x=rand_x,
y=rand_y)

width = 400
height = 400

rand_x = randint(0 + 50, width - 100)
rand_y = randint(0 + 50, height - 100)

root = Tk()

btn = Button(text='Click ME!')
btn.place(x=rand_x,
y=rand_y)
btn.bind('<Enter>', magic)

root.geometry(str(width)+"x"+str(height))
root.mainloop()